package com.jshoperxms.action.mall.backstage.feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.PostsT;
import com.jshoperxms.entity.PostsZanRecordT;
import com.jshoperxms.entity.UserT;
import com.jshoperxms.service.PostsTService;
import com.jshoperxms.service.PostsZanRecordTService;
import com.jshoperxms.service.UsertService;
import com.jshoperxms.service.impl.Serial;

@Namespace("/mall/feedback/postszanrecord")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class PostsZanRecordTAction extends BaseTAction {

	@Resource
	private PostsZanRecordTService postsZanRecordTService;

	@Resource
	private UsertService usertService;

	@Resource
	private PostsTService postsTService;

	/**
	 * 保存帖子点赞参数
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String savePostsZanRecordT() {
		Criterion criterion = Restrictions.eq("userid", BaseTools.getAdminCreateId());
		UserT usert = this.usertService.findOneByCriteria(UserT.class, criterion);
		PostsZanRecordT pzrt = new PostsZanRecordT();
		pzrt.setId(this.getSerial().Serialid(Serial.POSTSZANRECORDT));
		pzrt.setPostsid(this.getPostsId());
		pzrt.setLoginname(usert.getRealname());
		pzrt.setUserid(usert.getUserid());
		pzrt.setNick(usert.getRealname());
		pzrt.setHeadpath(usert.getHeadpath());
		pzrt.setStatus(DataUsingState.USING.getState());
		pzrt.setUpdatetime(BaseTools.getSystemTime());
		pzrt.setCreatetime(BaseTools.getSystemTime());
		pzrt.setVersiont(1);
		this.postsZanRecordTService.save(pzrt);
		// 帖子评论数+1
		PostsT postsT = this.postsTService.findByPK(PostsT.class, this.getPostsId());
		if (postsT != null) {
			postsT.setZannum(postsT.getZannum() + 1);
			this.postsTService.update(postsT);
		}
		this.setSucflag(true);
		return JSON;
	}
	
	/**
	 * 删除帖子点赞参数
	 * @return
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delPostsZanRecordT() {
		PostsZanRecordT pzrt = this.postsZanRecordTService.findByPK(PostsZanRecordT.class, this.getPostsZanRecordId());
		if(pzrt != null){
			pzrt.setUpdatetime(BaseTools.getSystemTime());
			pzrt.setStatus(DataUsingState.DEL.getState());
			this.postsZanRecordTService.update(pzrt);
			// 帖子评论数+1
			PostsT postsT = this.postsTService.findByPK(PostsT.class, this.getPostsId());
			if (postsT != null) {
				postsT.setZannum(postsT.getZannum() - 1);
				this.postsTService.update(postsT);
			}
		}
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 查询用户是否点赞
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findPostsZanRecordT() {
		Criterion criterion = Restrictions.and(Restrictions.eq("postsid", this.getPostsId())).add(Restrictions.eq("userid", BaseTools.getAdminCreateId())).add(Restrictions.ne("status", DataUsingState.DEL.getState()));
		PostsZanRecordT pzrt = this.postsZanRecordTService.findOneByCriteria(PostsZanRecordT.class, criterion);
		if(pzrt != null){
			bean = pzrt;
			this.setLike(true);
		}
		this.setSucflag(true);
		return JSON;
	}
	
	private boolean isLike = false;
	private String postsId;
	private String ids;
	private String postsZanRecordId;
	private PostsZanRecordT bean;
	private List<PostsZanRecordT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	public PostsZanRecordTService getPostsZanRecordTService() {
		return postsZanRecordTService;
	}

	public void setPostsZanRecordTService(PostsZanRecordTService postsZanRecordTService) {
		this.postsZanRecordTService = postsZanRecordTService;
	}

	public PostsZanRecordT getBean() {
		return bean;
	}

	public void setBean(PostsZanRecordT bean) {
		this.bean = bean;
	}

	public List<PostsZanRecordT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<PostsZanRecordT> beanlists) {
		this.beanlists = beanlists;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getPostsId() {
		return postsId;
	}

	public void setPostsId(String postsId) {
		this.postsId = postsId;
	}

	public String getPostsZanRecordId() {
		return postsZanRecordId;
	}

	public void setPostsZanRecordId(String postsZanRecordId) {
		this.postsZanRecordId = postsZanRecordId;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}

}
