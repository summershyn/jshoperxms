package com.jshoperxms.action.mall.backstage.member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.MemberGradeT;
import com.jshoperxms.service.MemberGradeTService;
import com.jshoperxms.service.impl.Serial;

@Namespace("/mall/member/membergrade")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class MemberGradeTAction extends BaseTAction {

	@Resource
	private MemberGradeTService memberGradeTService;

	/**
	 * 保存会员等级制度信息
	 * 
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String saveMemberGradeT() {
		MemberGradeT mgt = new MemberGradeT();
		mgt.setId(this.getSerial().Serialid(Serial.MEMBERGRADE));
		mgt.setType(this.getType());
		mgt.setCreatetime(BaseTools.getSystemTime());
		mgt.setCreatorid(StringUtils.trim(BaseTools.getAdminCreateId()));
		mgt.setDiscount(this.getDiscount());
		mgt.setEnd(this.getLevelrangeend());
		mgt.setIncrement(this.getIncrement());
		mgt.setMpchangepower(this.getMpchangepower());
		mgt.setMpstate(this.getMpstate());
		mgt.setMultiplypower(this.getMultiplypower());
		mgt.setName(this.getName());
		mgt.setStart(this.getLevelrangestart());
		mgt.setType(this.getType());
		mgt.setUpdatetime(BaseTools.getSystemTime());
		mgt.setVersiont(1);
		mgt.setStatus(DataUsingState.USING.getState());
		this.memberGradeTService.save(mgt);
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 查询所有会员等级制度信息
	 * 
	 * @return
	 */
	@Action(value = "findAll", results = { @Result(name = "json", type = "json") })
	public String findAllMemberGradeT() {
		Criterion criterion = Restrictions.eq("status", DataUsingState.USING.getState());
		List<MemberGradeT> lists = this.memberGradeTService.findByCriteria(MemberGradeT.class, criterion);
		if (!lists.isEmpty()) {
			beanlists = new ArrayList<MemberGradeT>();
			beanlists = lists;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 分页查询会员等级制度信息
	 * 
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findMemberGradeTByPage() {
//		Map<String, Object> params = ActionContext.getContext().getParameters();
//		for (Map.Entry<String, Object> entry : params.entrySet()) {
//			String key = entry.getKey();
//			Object value = entry.getValue();
//			System.out.println("key:" + key + " value: " + Arrays.toString((String[]) value));
//		}

		// 检测是否需要搜索内容
		this.findDefaultAllMemberGradeT();
		return JSON;

	}

	public void processMemberGradeTList(List<MemberGradeT> list) {
		for (Iterator<MemberGradeT> it = list.iterator(); it.hasNext();) {
			MemberGradeT mgt = it.next();
			Map<String, Object> cellMap = new HashMap<String, Object>();
			cellMap.put("id", mgt.getId());
			cellMap.put("name", mgt.getName());
			cellMap.put("type", BaseEnums.MemberGradeType.getName(mgt.getType()));
			cellMap.put("creatorid", mgt.getCreatorid());
			cellMap.put("updatetime", BaseTools.formateDbDate(mgt.getUpdatetime()));
			cellMap.put("status", BaseEnums.DataUsingState.getName(mgt.getStatus()));
			cellMap.put("versiont", mgt.getVersiont());
			data.add(cellMap);

		}
	}

	public void findDefaultAllMemberGradeT() {
		int currentPage = 1;
		if (this.getStart() != 0) {
			currentPage = this.getStart() / this.getLength() == 1 ? 2 : this.getStart() / this.getLength() + 1;
		}
		int lineSize = this.getLength();
		Criterion criterion = Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered = recordsTotal = this.memberGradeTService.countfindAll(MemberGradeT.class);
		List<MemberGradeT> list = this.memberGradeTService.findByCriteriaByPage(MemberGradeT.class, criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processMemberGradeTList(list);
	}

	/**
	 * 根据ID查询会员等级制度信息
	 * 
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findOneMemberGradeT() {
		if (StringUtils.isBlank(this.getMemberGradeId())) {
			return JSON;
		}
		MemberGradeT memberGradeT = this.memberGradeTService.findByPK(MemberGradeT.class, this.getMemberGradeId());
		if (memberGradeT != null) {
			bean = memberGradeT;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 修改会员等级制度信息
	 * 
	 * @return
	 */
	@Action(value = "update", results = { @Result(name = "json", type = "json") })
	public String updateMemberGradeT() {
		MemberGradeT mgt = this.memberGradeTService.findByPK(MemberGradeT.class, this.getMemberGradeId());
		if (mgt != null) {
			mgt.setType(this.getType());
			mgt.setDiscount(this.getDiscount());
			mgt.setEnd(this.getLevelrangeend());
			mgt.setIncrement(this.getIncrement());
			mgt.setMpchangepower(this.getMpchangepower());
			mgt.setMpstate(this.getMpstate());
			mgt.setMultiplypower(this.getMultiplypower());
			mgt.setName(this.getName());
			mgt.setStart(this.getLevelrangestart());
			mgt.setType(this.getType());
			mgt.setUpdatetime(BaseTools.getSystemTime());
			mgt.setVersiont(mgt.getVersiont() + 1);
			mgt.setStatus(this.getStatus());
			this.memberGradeTService.update(mgt);
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 删除会员等级制度信息
	 * 
	 * @return
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delMemberGradeT() {
		String ids[] = StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for (String s : ids) {
			MemberGradeT mgt = this.memberGradeTService.findByPK(MemberGradeT.class, s);
			if (mgt != null) {
				mgt.setUpdatetime(BaseTools.getSystemTime());
				mgt.setStatus(BaseEnums.DataUsingState.DEL.getState());
				this.memberGradeTService.update(mgt);
			}
		}
		this.setSucflag(true);
		return JSON;
	}

	private String ids;
	private String memberGradeId;
	private MemberGradeT bean;
	private List<MemberGradeT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	private String name;
	private double levelrangestart;
	private double levelrangeend;
	private double increment;
	private double multiplypower;
	private double discount;
	private double mpchangepower;
	private String mpstate;
	private String type;
	private String status;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getMemberGradeId() {
		return memberGradeId;
	}

	public void setMemberGradeId(String memberGradeId) {
		this.memberGradeId = memberGradeId;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLevelrangestart() {
		return levelrangestart;
	}

	public void setLevelrangestart(double levelrangestart) {
		this.levelrangestart = levelrangestart;
	}

	public double getLevelrangeend() {
		return levelrangeend;
	}

	public void setLevelrangeend(double levelrangeend) {
		this.levelrangeend = levelrangeend;
	}

	public double getIncrement() {
		return increment;
	}

	public void setIncrement(double increment) {
		this.increment = increment;
	}

	public double getMultiplypower() {
		return multiplypower;
	}

	public void setMultiplypower(double multiplypower) {
		this.multiplypower = multiplypower;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getMpchangepower() {
		return mpchangepower;
	}

	public void setMpchangepower(double mpchangepower) {
		this.mpchangepower = mpchangepower;
	}

	public String getMpstate() {
		return mpstate;
	}

	public void setMpstate(String mpstate) {
		this.mpstate = mpstate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MemberGradeT getBean() {
		return bean;
	}

	public void setBean(MemberGradeT bean) {
		this.bean = bean;
	}

	public List<MemberGradeT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<MemberGradeT> beanlists) {
		this.beanlists = beanlists;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
