package com.jshoperxms.action.mall.backstage.member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.SHA1;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.MemberGroupT;
import com.jshoperxms.entity.MemberMgRpT;
import com.jshoperxms.entity.MemberT;
import com.jshoperxms.service.MemberGroupTService;
import com.jshoperxms.service.MemberMgRpTService;
import com.jshoperxms.service.MemberTService;
import com.jshoperxms.service.impl.Serial;

@Namespace("/mall/member")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class MemberTAction extends BaseTAction {

	@Resource
	private MemberTService memberTService;

	@Resource
	private MemberGroupTService memberGroupTService;

	@Resource
	private MemberMgRpTService memberMgRpTService;
	
	/**
	 * 保存会员信息
	 * 
	 * @return
	 */
	@Action(value = "save", results = { @Result(type = "json", name = "json") })
	public String saveMemberT() {
		MemberT mt = new MemberT();
		mt.setId(this.getSerial().Serialid(Serial.MEMBER));
		mt.setBirthday(this.getBirthday());
		mt.setBlood(this.getBlood());
		mt.setCity(this.getCity());
		mt.setConstellation(this.getConstellation());
		mt.setCreatorid(StringUtils.trim(BaseTools.getAdminCreateId()));
		mt.setDes(this.getDes());
		mt.setDistrict(this.getDistrict());
		mt.setEmail(this.getEmail());
		mt.setHeadpath(this.getHeadpath());
		mt.setLoginname(this.getLoginname());
		mt.setLoginpwd(SHA1.getDigestOfString(this.getLoginpwd()));
		mt.setMemberGroupId(this.getMemberGroupId());
		MemberGroupT mgt = this.memberGroupTService.findByPK(MemberGroupT.class, this.getMemberGroupId());
		mt.setMemberGroupName(mgt.getName());
		mt.setMemberstate(this.getMemberstate());
		mt.setMerrystatus(this.getMerrystatus());
		mt.setMobile(this.getMobile());
		mt.setNick(this.getNick());
		mt.setProvince(this.getProvince());
		mt.setQq(this.getQq());
		mt.setRealname(this.getRealname());
		mt.setSex(this.getSex());
		mt.setSinaweibo(this.getSinaweibo());
		mt.setStatus(DataUsingState.USING.getState());
		mt.setTag(this.getTag());
		mt.setTelno(this.getTelno());
		mt.setWeixin(this.getWeixin());
		mt.setWhichsex(this.getWhichsex());
		mt.setBelove(0);
		mt.setLoveother(0);
		mt.setCreatetime(BaseTools.getSystemTime());
		mt.setUpdatetime(BaseTools.getSystemTime());
		mt.setIntegration(0);
		mt.setPostingcount(0);
		mt.setVersiont(0);
		this.memberTService.save(mt);
		if(mt.getId() != null){
			MemberMgRpT memberMgRpT = new MemberMgRpT();
			memberMgRpT.setCreatetime(BaseTools.getSystemTime());
			memberMgRpT.setId(this.getSerial().Serialid(Serial.MEMBERMGRPT));
			memberMgRpT.setMembergradeid(this.getMembergradeid());
			memberMgRpT.setMemberid(mt.getId());
			memberMgRpT.setStatus(DataUsingState.USING.getState());
			memberMgRpT.setUpdatetime(BaseTools.getSystemTime());
			memberMgRpT.setVersiont(1);
			this.memberMgRpTService.save(memberMgRpT);
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 查询所有会员信息
	 * 
	 * @return
	 */
	@Action(value = "findAll", results = { @Result(type = "json", name = "json") })
	public String findAllMemberT() {
		Criterion criterion = Restrictions.eq("status", DataUsingState.USING.getState());
		List<MemberT> list = this.memberTService.findByCriteria(MemberT.class, criterion);
		if (!list.isEmpty()) {
			beanlists = new ArrayList<MemberT>();
			beanlists = list;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 分页查询
	 * 
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(type = "json", name = "json") })
	public String findMemberTByPage() {
//		Map<String, Object> params = ActionContext.getContext().getParameters();
//		for (Map.Entry<String, Object> entry : params.entrySet()) {
//			String key = entry.getKey();
//			Object value = entry.getValue();
//			System.out.println("key:" + key + " value: " + Arrays.toString((String[]) value));
//		}
		// 检测是否需要搜索内容
		this.findDefaultAllMemberT();
		return JSON;
	}

	public void processMemberTList(List<MemberT> list) {
		for (Iterator<MemberT> it = list.iterator(); it.hasNext();) {
			MemberT mt = it.next();
			Map<String, Object> cellMap = new HashMap<String, Object>();
			cellMap.put("id", mt.getId());
			cellMap.put("nick", mt.getNick());
			cellMap.put("loginname", mt.getLoginname());
			cellMap.put("realname", mt.getRealname());
			cellMap.put("sex", BaseEnums.MemberSex.getName(mt.getSex()));
			cellMap.put("mobile", mt.getMobile());
			cellMap.put("memberstate", BaseEnums.MemberState.getName(mt.getMemberstate()));
			cellMap.put("updatetime", BaseTools.formateDbDate(mt.getUpdatetime()));
			cellMap.put("status", BaseEnums.DataUsingState.getName(mt.getStatus()));
			cellMap.put("versiont", mt.getVersiont());
			data.add(cellMap);
		}
	}

	public void findDefaultAllMemberT() {
		int currentPage = 1;
		if (this.getStart() != 0) {
			currentPage = this.getStart() / this.getLength() == 1 ? 2 : this.getStart() / this.getLength() + 1;
		}
		int lineSize = this.getLength();
		Criterion criterion = Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered = recordsTotal = this.memberTService.count(MemberT.class, criterion).intValue();
		List<MemberT> list = this.memberTService.findByCriteriaByPage(MemberT.class, criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processMemberTList(list);
	}

	/**
	 * 根据会员ID查询会员信息
	 * 
	 * @return
	 */
	@Action(value = "find", results = { @Result(type = "json", name = "json") })
	public String findOneMemberT() {
		if (StringUtils.isBlank(this.getMemberId())) {
			return JSON;
		}
		MemberT memberT = this.memberTService.findByPK(MemberT.class, this.getMemberId());
		Criterion criterion = Restrictions.and(Restrictions.eq("memberid", this.getMemberId())).add(Restrictions.ne("status", DataUsingState.DEL.getState()));
		MemberMgRpT mt = this.memberMgRpTService.findOneByCriteria(MemberMgRpT.class, criterion);
		if (memberT != null) {
			bean = memberT;
			memberMgRpT = mt;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 修改会员信息
	 * @return
	 */
	@Action(value = "update", results = { @Result(type = "json", name = "json") })
	public String updateMemberT() {
		MemberT memberT = this.memberTService.findByPK(MemberT.class, this.getMemberId());
		if(memberT != null){
			memberT.setBirthday(this.getBirthday());
			memberT.setBlood(this.getBlood());
			memberT.setCity(this.getCity());
			memberT.setConstellation(this.getConstellation());
			memberT.setDes(this.getDes());
			memberT.setDistrict(this.getDistrict());
			memberT.setEmail(this.getEmail());
			memberT.setHeadpath(this.getHeadpath());
			memberT.setLoginname(this.getLoginname());
			memberT.setLoginpwd(SHA1.getDigestOfString(this.getLoginpwd()));
			memberT.setMemberGroupId(this.getMemberGroupId());
			MemberGroupT mgt = this.memberGroupTService.findByPK(MemberGroupT.class, this.getMemberGroupId());
			memberT.setMemberGroupName(mgt.getName());
			memberT.setMemberstate(this.getMemberstate());
			memberT.setMerrystatus(this.getMerrystatus());
			memberT.setMobile(this.getMobile());
			memberT.setNick(this.getNick());
			memberT.setQq(this.getQq());
			memberT.setRealname(this.getRealname());
			memberT.setSex(this.getSex());
			memberT.setSinaweibo(this.getSinaweibo());
			memberT.setStatus(this.getStatus());
			memberT.setTag(this.getTag());
			memberT.setTelno(this.getTelno());
			memberT.setWeixin(this.getWeixin());
			memberT.setWhichsex(this.getWhichsex());
			memberT.setBelove(0);
			memberT.setLoveother(0);
			memberT.setUpdatetime(BaseTools.getSystemTime());
			memberT.setIntegration(0);
			memberT.setPostingcount(0);
			memberT.setVersiont(memberT.getVersiont() + 1);
			this.memberTService.update(memberT);
			Criterion criterion = Restrictions.and(Restrictions.eq("memberid", this.getMemberId())).add(Restrictions.ne("status", DataUsingState.DEL.getState()));
			MemberMgRpT mt = this.memberMgRpTService.findOneByCriteria(MemberMgRpT.class, criterion);
			if(mt != null){
				mt.setMembergradeid(this.getMembergradeid());
				mt.setUpdatetime(BaseTools.getSystemTime());
				mt.setVersiont(mt.getVersiont() + 1);
				this.memberMgRpTService.update(mt);
			}
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 删除会员信息
	 * @return
	 */
	@Action(value = "del", results = { @Result(type = "json", name = "json") })
	public String delMemberGroupT() {
		String ids[]=StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for (String s : ids) {
			MemberT memberT = this.memberTService.findByPK(MemberT.class, s);
			memberT.setStatus(BaseEnums.DataUsingState.DEL.getState());
			memberT.setUpdatetime(BaseTools.getSystemTime());
			this.memberTService.update(memberT);
		}
		this.setSucflag(true);
		return JSON;
	}
	
	private String ids;
	private String memberId;
	private MemberMgRpT memberMgRpT;
	private MemberT bean;
	private List<MemberT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	private String birthday;
	private String blood;
	private String city;
	private String constellation;
	private String des;
	private String district;
	private String email;
	private String headpath;
	private String loginname;
	private String loginpwd;
	private String memberGroupId;
	private String memberGroupName;
	private String memberstate;
	private String merrystatus;
	private String mobile;
	private String nick;
	private String province;
	private String qq;
	private String realname;
	private String sex;
	private String sinaweibo;
	private String status;
	private String tag;
	private String telno;
	private String weixin;
	private String whichsex;
	private int belove;
	private int loveother;
	private int integration;
	private int postingcount;
	private int versiont;
	private String membergradeid;
	
	
	public MemberTService getMemberTService() {
		return memberTService;
	}

	public void setMemberTService(MemberTService memberTService) {
		this.memberTService = memberTService;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public MemberT getBean() {
		return bean;
	}

	public void setBean(MemberT bean) {
		this.bean = bean;
	}

	public List<MemberT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<MemberT> beanlists) {
		this.beanlists = beanlists;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBlood() {
		return blood;
	}

	public void setBlood(String blood) {
		this.blood = blood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getConstellation() {
		return constellation;
	}

	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHeadpath() {
		return headpath;
	}

	public void setHeadpath(String headpath) {
		this.headpath = headpath;
	}

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getLoginpwd() {
		return loginpwd;
	}

	public void setLoginpwd(String loginpwd) {
		this.loginpwd = loginpwd;
	}

	public String getMemberGroupId() {
		return memberGroupId;
	}

	public void setMemberGroupId(String memberGroupId) {
		this.memberGroupId = memberGroupId;
	}

	public String getMemberGroupName() {
		return memberGroupName;
	}

	public void setMemberGroupName(String memberGroupName) {
		this.memberGroupName = memberGroupName;
	}

	public String getMemberstate() {
		return memberstate;
	}

	public void setMemberstate(String memberstate) {
		this.memberstate = memberstate;
	}

	public String getMerrystatus() {
		return merrystatus;
	}

	public void setMerrystatus(String merrystatus) {
		this.merrystatus = merrystatus;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSinaweibo() {
		return sinaweibo;
	}

	public void setSinaweibo(String sinaweibo) {
		this.sinaweibo = sinaweibo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTelno() {
		return telno;
	}

	public void setTelno(String telno) {
		this.telno = telno;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getWhichsex() {
		return whichsex;
	}

	public void setWhichsex(String whichsex) {
		this.whichsex = whichsex;
	}

	public int getBelove() {
		return belove;
	}

	public void setBelove(int belove) {
		this.belove = belove;
	}

	public int getLoveother() {
		return loveother;
	}

	public void setLoveother(int loveother) {
		this.loveother = loveother;
	}

	public int getIntegration() {
		return integration;
	}

	public void setIntegration(int integration) {
		this.integration = integration;
	}

	public int getPostingcount() {
		return postingcount;
	}

	public void setPostingcount(int postingcount) {
		this.postingcount = postingcount;
	}

	public int getVersiont() {
		return versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getMembergradeid() {
		return membergradeid;
	}

	public void setMembergradeid(String membergradeid) {
		this.membergradeid = membergradeid;
	}

	public MemberMgRpT getMemberMgRpT() {
		return memberMgRpT;
	}

	public void setMemberMgRpT(MemberMgRpT memberMgRpT) {
		this.memberMgRpT = memberMgRpT;
	}

}
