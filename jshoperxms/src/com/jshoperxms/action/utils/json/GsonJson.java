package com.jshoperxms.action.utils.json;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
/**
* Created with sdywcd@gmail.com
* Author:sdywcd@gmail.com
* Date:15/7/23
* Time:下午3:40
* Note:Gson和Mysql的日期转换方法
**/
public class GsonJson {
	
	public static String parseDataToJson(Object obj) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Timestamp.class, new TimestampTypeAdapter()).registerTypeAdapter(java.sql.Date.class, new SQLDateTypeAdapter()).create();
		return gson.toJson(obj);
	}

	public static <T> List<T> parseJsonToData(String json,Class<T> t){
		List<T>list=new ArrayList<T>();
		JsonArray array=new JsonParser().parse(json).getAsJsonArray();
		for(final JsonElement elem:array){
			list.add(new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Timestamp.class, new TimestampTypeAdapter()).registerTypeAdapter(java.sql.Date.class, new SQLDateTypeAdapter()).create().fromJson(elem, t));
		}
		return list;
	}
}
