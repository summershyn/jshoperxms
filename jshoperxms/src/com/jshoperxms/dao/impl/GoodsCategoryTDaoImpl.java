package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.GoodsCategoryTDao;
import com.jshoperxms.entity.GoodsCategoryT;


@Repository("goodsCategoryTDao")
public class GoodsCategoryTDaoImpl extends BaseTDaoImpl<GoodsCategoryT> implements GoodsCategoryTDao {
	

	private static final Logger log = LoggerFactory.getLogger(GoodsCategoryTDaoImpl.class);

	
}
