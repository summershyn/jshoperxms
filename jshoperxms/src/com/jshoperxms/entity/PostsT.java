package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the posts_t database table.
 * 
 */
@Entity
@Table(name="posts_t")
@NamedQuery(name="PostsT.findAll", query="SELECT p FROM PostsT p")
public class PostsT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date activetime;

	private String bigimageurl;

	private String content;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private String headpath;

	private String loginname;

	private String nick;

	private int replynum;

	private String smallimageurl;

	private String status;

	private String title;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private String userid;

	private int versiont;

	private int zannum;

	public PostsT() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getActivetime() {
		return this.activetime;
	}

	public void setActivetime(Date activetime) {
		this.activetime = activetime;
	}

	public String getBigimageurl() {
		return this.bigimageurl;
	}

	public void setBigimageurl(String bigimageurl) {
		this.bigimageurl = bigimageurl;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getHeadpath() {
		return this.headpath;
	}

	public void setHeadpath(String headpath) {
		this.headpath = headpath;
	}

	public String getLoginname() {
		return this.loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getReplynum() {
		return this.replynum;
	}

	public void setReplynum(int replynum) {
		this.replynum = replynum;
	}

	public String getSmallimageurl() {
		return this.smallimageurl;
	}

	public void setSmallimageurl(String smallimageurl) {
		this.smallimageurl = smallimageurl;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getVersiont() {
		return this.versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

	public int getZannum() {
		return this.zannum;
	}

	public void setZannum(int zannum) {
		this.zannum = zannum;
	}

}