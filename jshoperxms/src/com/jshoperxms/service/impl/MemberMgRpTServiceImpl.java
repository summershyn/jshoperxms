package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.MemberMgRpT;
import com.jshoperxms.service.MemberMgRpTService;

@Service("memberMgRpTService")
@Scope("prototype")
public class MemberMgRpTServiceImpl extends BaseTServiceImpl<MemberMgRpT> implements MemberMgRpTService {

}
