package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.RoleM;
import com.jshoperxms.service.RoleMService;

@Service("roleMService")
@Scope("prototype")
public class RoleMServiceImpl extends BaseTServiceImpl<RoleM>implements RoleMService {
	
}
