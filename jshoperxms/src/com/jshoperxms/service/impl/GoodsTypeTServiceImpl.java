package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsTypeT;
import com.jshoperxms.service.GoodsTypeTService;

@Service("goodsTypeTService")
@Scope("prototype")
public class GoodsTypeTServiceImpl extends BaseTServiceImpl<GoodsTypeT> implements GoodsTypeTService {

}
